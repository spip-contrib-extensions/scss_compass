<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'compass_description' => 'Permet d\'utiliser le framework Compass dans vos fichiers .scss gérés par le plugin SCSS. Une fois installé, on peut utiliser <pre>@import "compass";</pre> dans un fichier .scss pour importer l\'ensemble du framework. On pourra alors utiliser les classes, fonctions et autres mixins de <a href="http://compass-style.org/">Compass</a>.',
	'compass_slogan' => 'Utilisez le framework CSS Compass dans SPIP !',
);
