<?php
/**
 * Options du plugin Compass au chargement
 *
 * @plugin     Compass
 * @copyright  2014
 * @author     bystrano
 * @licence    GNU/GPL
 * @package    SPIP\Compass\Options
 */

if (!defined('_ECRIRE_INC_VERSION')) return;

_chemin(find_in_path('lib/scssphp-compass/stylesheets'));
